##描述

在实际工作中，发现Redis的主节点之间的数据同步，没有比较好用的现成工具，所以就写了一个。

该开源项目，主要是用于在Nginx+Lua的环境下，通过内核消息队列的方式，需要同步的Redis命令写入消息队列，然后由在同一台机器上的同步Server进行数据的异步多写。

分为两部分：

server     同步server，从内核消息队列读命令，把数据同步到其他Redis节点 

lua_ext    Lua扩展，用于给lua脚本调用，把需要同步的Redis命令写入到内核消息队列中


###启动同步服务器:
           
redis_sync_server -H /usr/local/syncserver -d

-d表示以守护进程运行

-H表示设定程序的home目录，即编译的时候通过CMAKE_INSTALL_PREFIX指定的目录

###Lua脚本使用例子:

	package.cpath = '/usr/local/lib/lua/5.1/?.so;/usr/local/openresty/lualib/?.so;;'  

	local syncr = require("libsyncr") 
	local msgid, err = syncr.sync_init()
	if msgid < 0 then
        ngx.log(ngx.ERR, "sync_init failed : ", err)
        return
    end
	local ok, err = syncr.sync_command(msgid,"set bcc 8907")
    if not ok then
        ngx.log(ngx.ERR, "sync_command failed : ", err)
        return
    end

###配置:

	[log]
	config_file=${SYNC_SERVER_HOME}/etc/log.conf

	[server]
	pid_file=${SYNC_SERVER_HOME}/logs/sync.pid
	cwd=${SYNC_SERVER_HOME}

	[redis]
	sync_nodes_list=10.255.209.66:6379
	thread_num=1
	timeout_secs=1


###依赖的库:

	jemalloc -- 用于替换malloc
	zlog     -- 日志打印
    hiredis  -- 访问Redis的客户端

##编译安装

使用cmake进行编译

进入各个子目录，新建build目录，进入build目录再执行cmake && make && make install

鼓励直接修改CMakeLists达到自己的个性化编译要求，也可以联系我

lua_ext 默认安装到/usr/local/openresty/lualib/目录下，可以改

server 默认安装到/usr/local目录下，建议通过设置CMAKE_INSTALL_PREFIX来修改


##联系开发者

堂吉诃德  
421093703@qq.com
http://my.oschina.net/xuhh